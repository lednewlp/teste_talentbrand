class NotesController < ApplicationController
  before_action :set_note, only: [:show, :edit, :update, :destroy]
  before_action  :authenticate_user!

  def index
    @notes = Note.all.order(created_at: :DESC)
  end

  def search
    @notes = Note.search(search_params[:q])
  end

  def show
  end

  def new
    @note = Note.new
  end

  def edit
  end


  def create
    @note = Note.new(note_params)

      if @note.save
       redirect_to @note, notice: 'Anotação criada com sucesso.'
      else
        flash.now[:alert] = @note.errors.full_messages.to_sentence
        render :new
      end
  end


  def update
    if @note.update(note_params)
      redirect_to @note, notice: 'Anotação atualizada com sucesso.'
    else
      flash.now[:alert] = @note.errors.full_messages.to_sentence
      render :edit
    end
  end


  def destroy
    @note.destroy
      redirect_to notes_url, notice: 'Anotação deletada com sucesso.'
  end

  private

  def set_note
    @note = Note.friendly.find(params[:id])
  end

  def note_params
    params.require(:note).permit(:title, :note, :data, :status)
  end

  def search_params
    params.permit(:q)
  end
end
