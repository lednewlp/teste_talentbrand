class Note < ApplicationRecord
  include PgSearch
  extend FriendlyId

  validates :title, :note, :status, presence: true

  STATUS = %w{ Alta Média Baixa }

  friendly_id :title, use: :slugged


  pg_search_scope :search,
   against: %i[title note status]

   def symbol
     case prioridade
     when 'Alta' then 'Prioridade Alta'
     when 'Média' then 'Prioridade Media'
     when 'Baixa' then 'Prioridade Baixa'
     end
   end

   def css_color
     case prioridade
     when 'Alta' then 'danger'
     when 'Média' then 'warning'
     when 'Baixa' then 'success'
     end
   end


  private

  def prioridade
    if status == 'Alta'
      'Alta'
    else
      if status == 'Média'
        'Média'
      else
        'Baixa'
      end
    end
  end

end
