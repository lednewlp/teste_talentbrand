class AddSlugColumnToNotes < ActiveRecord::Migration[5.1]
  def change
    add_column :notes, :slug, :string, unique: true
  end
end
