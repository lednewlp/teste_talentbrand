Rails.application.routes.draw do
  devise_for :users
  root 'notes#index'

  resources :notes do
   get :search, on: :collection
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
